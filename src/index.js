import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

window.globalConf = {
    allowTmpStorage: true,
    allowedRequests: ["audiences", "attributes", "pages", "projects","events"], // Remove any entry to disallow app to do any of those requests // use lowercase
    limitRequests: {// limitRequests always uses data stored request response if available (so only first time request is really sent, after that local copy)
        attributes: false,
        audiences: false,
        pages: false,
        projects: false,
        events: false
    },
    token: {
        //default: '2:12345678abcdefghijklmnop', // enter your default token and set force to true to speed up
        force: false, // will skip the input field at the beginning and use the defaultToken
    },
    identifyingKey: { // key used as a headline in lists
        attributes: "name",
        audiences: "name",
        pages: "name",
        events: "name",
    },
    getApiUrl: getApiUrl,
    firstUppercase: firstUppercase,
}

function getApiUrl(mode, params) {
    if (window.globalConf.allowedRequests.indexOf(mode.toLowerCase()) === -1) {
        console.error(`Request type ${mode} is not allowed`);
        return '';
    }
    let paramString = '';
    if (typeof params === "object") {
        paramString += '?';
        for (var prop in params) {
            paramString += prop + '=' + params[prop] + '&';
        }
    }
    return 'https://api.optimizely.com/v2/' + mode.toLowerCase() + paramString;
}

function firstUppercase(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

ReactDOM.render( <App /> , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();