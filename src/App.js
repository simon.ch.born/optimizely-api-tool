import React, { Component } from 'react';
import './App.css';
import { Projects } from './views/Projects';
import { ProjectDetail } from './views/ProjectDetail';
import { TokenForm } from './views/TokenForm';
import { StatusBar } from './views/StatusBar';
import { Settings } from './views/Settings';
import '../node_modules/bootstrap/dist/css/bootstrap-grid.min.css';
import '../node_modules/bootstrap/dist/css/bootstrap-reboot.min.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap.bundle.min';
import { OauthSender } from 'react-oauth-flow';
import { OauthDummy } from './views/OauthDummy';
import { OauthSenderDummy } from './views/OauthSenderDummy';

class App extends Component {
  constructor(props) {
    super(props);

    this.tokenFieldId = 'enterToken';

    this.state = {
      mode: 'TokenForm',
      detailMode: '',
      verifiedToken: '',
      sourceProject: {
        id: 0,
        arrayId: 0,
        name: ''
      },
      targetProject: {
        id: 0,
        arrayId: 0,
        name: ''
      },
      includeArchived: false,
      propertiesToIncludeInApiCall: { // true when always included, array with modes if only true for those // TODO -> move to config
        attributes: {
          archived: true,
          condition_type: true,
          description: true,
          id: ["update"],
          key: true,
          last_modified: false,
          name: true
        },
        audiences: {
          archived: false,
          conditions: true,
          created: false,
          description: true,
          id: ["update"],
          is_classic: true,
          last_modified: false,
          name: true,
          segmentation: true
        },
        pages: {
          activation_type: true,
          archived: true,
          category: true,
          conditions: true,
          created: false,
          edit_url: true,
          id: ["update"],
          key: false,
          last_modified: false,
          name: true,
        },
        events: {
          archived: true,
          category: true,
          config: true,
          created: false,
          event_type: true,
          id: ["update"],
          is_classic: true,
          key: true,
          name: true,
          page_id: true
        },
      },
      urlAppendProperty: { // TODO -> move to config
        attributes: 'id',
        audiences: 'id',
        pages: 'id',
        events: 'id',
      }
    }

    this.setToken = this.setToken.bind(this);
    this.getComponent = this.getComponent.bind(this);
    this.switchModeTo = this.switchModeTo.bind(this);
    this.selectAsSource = this.selectAsSource.bind(this);
    this.selectAsTarget = this.selectAsTarget.bind(this);
    this.showProjectDetails = this.showProjectDetails.bind(this);
    this.toggleIncArchived = this.toggleIncArchived.bind(this);
  }

  componentDidMount() {
    if (window.globalConf.token.force && window.globalConf.token.default) {
      this.setToken(undefined, window.globalConf.token.default);
    }
  }

  getComponent() {
    switch (this.state.mode) {
      case 'TokenForm':
        return <TokenForm 
          onClick={this.setToken} 
          switchModeTo={this.switchModeTo}
          tokenFieldId={this.tokenFieldId} 
          includeArchived={this.state.includeArchived} />
      case 'OAuth': // TODO - add OAuth support
        console.log(process);
        //return <OauthDummy />
        return <OauthSenderDummy />
          /*return <OauthSender
          authorizeUrl="https://app.optimizely.com/oauth2/authorize"
          clientId="17059831777" //{process.env.CLIENT_ID}
          redirectUri="https://www.optimizely-tool.com/run" //{window.location.href}
          accountId="12761690192"
          clientSecret="VIjswoZPZQOi-dRClx1j1a0jdTOEX935CDmSkSA1FSA"
          /*state={{ from: '/settings' }}
          args={{scopes:"all", responseType:"token", accountId:""}}*/
          /*state={{test:"true"}}
          render={({ url }) => <a href={url}>Connect to Optimizely</a>} />*/
          
      case 'Projects':
        return <Projects
          showProjectDetails={this.showProjectDetails}
          token={this.state.verifiedToken}
          selectAsSource={this.selectAsSource}
          selectAsTarget={this.selectAsTarget}
          anyProjectSelected={this.state.sourceProject.id || this.state.targetProject.id} 
          switchModeTo={this.switchModeTo} />
      case 'ProjectDetail':
        return <ProjectDetail
          mode={this.state.detailMode}
          backFunc={this.switchModeTo}
          sourceProject={this.state.sourceProject}
          targetProject={this.state.targetProject}
          token={this.state.verifiedToken}
          showSettings={this.showSettings}
          includeArchived={this.state.includeArchived} 
          propertiesToIncludeInApiCall={this.state.propertiesToIncludeInApiCall} 
          urlAppendProperty={this.state.urlAppendProperty} />
      case 'Settings':
        return <Settings 
          includeArchived={this.state.includeArchived} 
          toggleIncArchived={this.toggleIncArchived} 
          switchModeTo={this.switchModeTo} />
      default:
        return <Projects />
    }
  }

  setToken(e, forceToken) {
    const verifiedToken = forceToken || document.getElementById(this.tokenFieldId).value;
    const storeTokenLocally = document.getElementById('storeTokenYes').checked;
    if (window.globalConf.allowTmpStorage && storeTokenLocally) { 
      localStorage.setItem('tmpStoreToken', verifiedToken); 
    }
    this.setState({ verifiedToken, mode: 'Projects' });
  }

  showProjectDetails(detailMode) {
    if (this.state.verifiedToken) {
      this.setState({ mode: 'ProjectDetail', detailMode });
    }
  }

  selectAsSource(arrayId, projData) {
    console.log(`selectAsSource called with arrayId: ${arrayId}`);
    this.setState({
      sourceProject: {
        id: projData[arrayId].id,
        name: projData[arrayId].name,
        arrayId: arrayId
      }
    });
  }

  selectAsTarget(arrayId, projData) {
    console.log(`selectAsTarget called with arrayId: ${arrayId}`);
    this.setState({
      targetProject: {
        id: projData[arrayId].id,
        name: projData[arrayId].name,
        arrayId: arrayId
      }
    });
  }

  switchModeTo(mode) {
    this.setState({ mode });
  }

  toggleIncArchived() {    
    this.setState({ includeArchived: !this.state.includeArchived });
  }

  render() {
    const mainContent = this.getComponent();

    return (
      <div className="wrapper">
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark static-top mb-3">
          <div className="container">
            <a className="navbar-brand" href="/">Optimizely API hyperspace communicator</a>
          </div>
        </nav>

        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-10">
              {mainContent}
              <div className="row">
                <div className="col-lg-12">
                </div>
              </div>
            </div>
            <div className="col-lg-2 border-left">
              <StatusBar
                sourceProject={this.state.sourceProject}
                targetProject={this.state.targetProject}
                includeArchived={this.state.includeArchived} 
                includedProperties={this.state.propertiesToIncludeInApiCall} 
                detailMode={this.state.detailMode} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;