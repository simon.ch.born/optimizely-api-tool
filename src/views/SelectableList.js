import React, { Component } from 'react';

export class SelectableList extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
        this.formattedObjectOutput = this.formattedObjectOutput.bind(this);
        this.getSelectButtons = this.getSelectButtons.bind(this);
    }

    formattedObjectOutput(data) {
        let returnString = '';
        if (typeof data !== 'object') {
            return data
        } else {
            for (var prop in data) {
                if (returnString !== '') { returnString += ' | '; }
                returnString += `{${prop}: ${data[prop]}}`;
            }
            return returnString;
        }
    }

    getSelectButtons() {
        if (!this.props.selectionAllowed) return ""; //(<span style={{ height: "54px", display: "block" }}>&nbsp;</span>);
        return (<div className="btn-group mb-2" role="group">
            <button className="btn btn-primary" type="submit" onClick={() => this.props.selectAll()}>Select all</button>
            <button className="btn btn-primary" type="submit" onClick={() => this.props.unselectAll()}>Unselect all</button>
        </div>);
    }

    render() {

        var self = this;

        if (!Array.isArray(this.props.data)) {
            return (<div>Error in SelectableList: Passed data is not an Array!</div>);
        }


        const output = this.props.data.map((el, idx) => {
            if (!this.props.selectionAllowed) {
                return (
                    <div key={idx} className="mb-3 border-bottom pb-2">
                        <h5>{el[window.globalConf.identifyingKey[this.props.mode]]}</h5>
                        {Object.entries(el).map(function (prop, propIdx) {
                            const val = self.formattedObjectOutput(prop[1]);
                            return (<div key={`key_${idx}${propIdx}`}><code className="text-dark">{`${prop[0]}: ${val}`}</code></div>);
                        })}
                    </div>);
            } else {
                const selectedClass = this.props.selectedEls.indexOf(idx) !== -1 ? 'selected' : '';
                return (
                    <div key={idx} className={`mb-3 border-bottom pb-2 selectableDiv ${selectedClass}`} onClick={() => this.props.toggleSelection(idx)}>
                        <h5>{el[window.globalConf.identifyingKey[this.props.mode]]}</h5>
                        {Object.entries(el).map(function (prop, propIdx) {
                            const val = self.formattedObjectOutput(prop[1]);
                            return (<div key={`key_${idx}${propIdx}`}><code className="text-dark">{`${prop[0]}: ${val}`}</code></div>);
                        })}
                    </div>);
            }
        });

        return (
            <div>
                {this.getSelectButtons()}
                <div className="col-lg-12">
                    {output}
                </div>
            </div>
        );
    }
}

export default SelectableList;