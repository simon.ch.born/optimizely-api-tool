import React, { Component } from 'react';

export class StatusBar extends Component {
    constructor(props) {
        super(props);

        this.getProjectInfoHtml = this.getProjectInfoHtml.bind(this);
    }

    getProjectInfoHtml(projObj) {
        if (!projObj || !projObj.id) {
            return (< div > < span > ...no project selected yet</span><br /> </div>);
        }
        return (<div>
            <span> Name: < span className="text-monospace" > {projObj.name} </span></span> <br />
            <span> Project ID: < span className="text-monospace" > {projObj.id} </span></span> <br />
            <span> Data Array Id: < span className="text-monospace" > {projObj.arrayId} </span></span >
        </div>
        );
    }

    render() {

        const sourceProjInfo = this.getProjectInfoHtml(this.props.sourceProject);
        const targetProjInfo = this.getProjectInfoHtml(this.props.targetProject);

        return (<div>
            <h3> Source: </h3> {sourceProjInfo}
            <h3> Target: </h3> {targetProjInfo}
            <hr />
            <h3> Showing archived: </h3> {this.props.includeArchived.toString()}
            <h3> Included for API calls:</h3>
            <pre> <code> {JSON.stringify(this.props.includedProperties[this.props.detailMode], undefined, 4)} </code></pre>
        </div>
        );
    }
}

export default StatusBar;