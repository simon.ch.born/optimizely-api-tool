import React, { Component } from 'react';
import { InfoAndSelection } from './InfoAndSelection';
import { ApiCallVerification } from './ApiCallVerification';
import Axios from 'axios';

export class ProjectDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mode: this.props.mode,
            sourceData: [],
            targetData: [],
            requestConfig: {
                headers: { 'Authorization': `Bearer ${this.props.token}` }
            },
            finishingApiCall: false,
            callMode: '',
            selectedEls: [],
            sendArray: [],
            modifiers: {}
        }

        this.getData = this.getData.bind(this);
        this.requestData = this.requestData.bind(this);
        this.setModeTo = this.setModeTo.bind(this);
        this.getDataForSourceAndTarget = this.getDataForSourceAndTarget.bind(this);
        this.applyFilters = this.applyFilters.bind(this);
        this.toggleSelection = this.toggleSelection.bind(this);
        this.create = this.create.bind(this);
        this.update = this.update.bind(this);
        this.assembleCallBody = this.assembleCallBody.bind(this);
        this.getComponent = this.getComponent.bind(this);
        this.selectAll = this.selectAll.bind(this);
        this.unselectAll = this.unselectAll.bind(this);
    }

    componentWillMount() {
        this.getDataForSourceAndTarget();
    }

    componentDidUpdate(prevProps) {
        if (this.props.includeArchived !== prevProps.includeArchived) {
            console.log('props have changed');
            this.getDataForSourceAndTarget();
        }
    }

    getDataForSourceAndTarget() {
        ["source", "target"].forEach((el) => this.getData(el));
    }

    requestData(projectId, projectSide) {
        return Axios.get(window.globalConf.getApiUrl(this.state.mode, { project_id: projectId, page: 1, per_page: 100 }), this.state.requestConfig)
            .then(response => {
                localStorage.setItem('tmpStore_' + this.state.mode + '_' + this.props[projectSide + "Project"].id, JSON.stringify(response.data));
                return response.data;
            })
            .catch(error => {
                console.log(error);
            });
    }

    getData(projectSide) {
        // console.log("get data called with mode " + this.state.mode + " and project " + projectSide);

        if (!this.props[projectSide + "Project"] || !this.props[projectSide + "Project"].id) return false;

        if (window.globalConf.limitRequests[this.state.mode] && localStorage.getItem('tmpStore_' + this.state.mode + '_' + this.props[projectSide + "Project"].id)) {

            console.log('using locally stored data for ' + this.state.mode + ' for project ' + this.props[projectSide + "Project"].name);

            var data = JSON.parse(localStorage.getItem('tmpStore_' + this.state.mode + '_' + this.props[projectSide + "Project"].id));
            this.setState({ [projectSide + "Data"]: this.applyFilters(data) });
            return;

        } else {
            console.log('requesting remote data for ' + this.state.mode + ' for project ' + this.props[projectSide + "Project"].name);

            this.requestData(this.props[projectSide + "Project"].id, projectSide)
                .then((res) => {
                    this.setState({ [projectSide + "Data"]: this.applyFilters(res) || [] });
                })
                .catch(error => {
                    console.log(error);
                });;
        }
    }

    applyFilters(input) {
        if (Array.isArray(input)) {
            if (!this.props.includeArchived) {

                input = input.filter((el) => !el.archived);
            }
        }
        return input;
    }

    setModeTo(mode) {
        if (window.globalConf.allowedRequests.indexOf(mode) !== -1) {
            this.setState({
                mode,
                sourceData: [],
                targetData: [],
                finishingApiCall: false
            },
                () => this.getDataForSourceAndTarget()
            );
        }
    }

    toggleSelection(idx) {
        let newArr = this.state.selectedEls;
        if (this.state.selectedEls.indexOf(idx) === -1) {
            newArr.push(idx);
            newArr.sort();
            this.setState({ selectedEls: newArr });
        } else {
            newArr.splice(newArr.indexOf(idx), 1);
            this.setState({ selectedEls: newArr });
        }
    }

    selectAll() {
        console.log('trying to select all');
        console.log(this.state);
        console.log(this.props);

        let selectedEls = [];
        for (var i = 0; i<this.state.sourceData.length; i++){
            selectedEls.push(i);
        }
        this.setState({selectedEls: selectedEls});
    }
    
    unselectAll() {
        this.setState({selectedEls: []});
    }


    assembleCallBody(includeProjectId) {
        let outputArray = [];
        console.log(`assembleCallBody called with source: ${this.props.sourceProject.name} (${this.props.sourceProject.id}) - target: ${this.props.targetProject.name} (${this.props.targetProject.id})`);
        this.state.selectedEls.forEach((idx) => {
            let addObj = {};
            for (var prop in this.state.sourceData[idx]) {
                const includeConfig = this.props.propertiesToIncludeInApiCall[this.state.mode][prop];
                console.log(includeConfig);
                console.log('callMode: ' + this.props.callMode);

                if (includeConfig && (includeConfig === true || includeConfig.indexOf(this.props.callMode) !== -1)) {
                    console.log('evaluated as true');

                    addObj[prop] = this.state.sourceData[idx][prop];
                } else if (prop === 'project_id' && includeProjectId) {
                    addObj[prop] = this.props.targetProject.id;
                }
            }
            outputArray.push(addObj);
        });
        console.log(outputArray);
        return outputArray;
    }

    create() {
        const outputArray = this.assembleCallBody(true);
        this.setState({ finishingApiCall: true, callMode: 'create', sendArray: outputArray });
    }

    update() {
        const outputArray = this.assembleCallBody(false);
        this.setState({ finishingApiCall: true, callMode: 'update', sendArray: outputArray });
    }

    getComponent() {
        if (this.state.finishingApiCall) {
            return <ApiCallVerification
                setModeTo={this.setModeTo}
                detailMode={this.state.mode}
                selectedEls={this.state.selectedEls}
                sourceProject={this.props.sourceProject}
                targetProject={this.props.targetProject}
                backFunc={this.props.backFunc}
                sourceData={this.state.sourceData}
                targetData={this.state.targetData}
                sendArray={this.state.sendArray}
                modifiers={this.state.modifiers}
                token={this.props.token}
                callMode={this.state.callMode}
                urlAppendProperty={this.props.urlAppendProperty} />
            } else {
                return <InfoAndSelection
                setModeTo={this.setModeTo}
                toggleSelection={this.toggleSelection}
                detailMode={this.state.mode}
                selectedEls={this.state.selectedEls}
                sourceProject={this.props.sourceProject}
                targetProject={this.props.targetProject}
                selectAll={this.selectAll}
                unselectAll={this.unselectAll}
                backFunc={this.props.backFunc}
                sourceData={this.state.sourceData}
                targetData={this.state.targetData}
                create={this.create}
                update={this.update} />
        }
    }

    render() {
        return this.getComponent();
    }
}

export default ProjectDetail;