import React, { Component } from 'react';
import { OauthSenderDummy } from './OauthSenderDummy';

export class TokenForm extends Component {
    constructor(props) {
        super(props);
        // disco
        this.state = {
            defaultTokenValue: localStorage.getItem('tmpStoreToken') || ''
        };
    }

    render() {
        return (<div>
            <div className="btn-group">
                <button className="btn btn-primary" onClick={() => this.props.switchModeTo('Settings')}>Settings</button>
            </div>
            <hr/>
            <form>
                <div className="form-group">
                    <label htmlFor="enterToken">Enter Token</label>
                    <input type="text" className="form-control" id={this.props.tokenFieldId} aria-describedby="tokenHelp" placeholder="Enter Optimizely API token" defaultValue={this.state.defaultTokenValue} />
                    <small id="tokenHelp" className="form-text text-muted">Enter the token you generated in the Optimizely GUI (admin rights required).</small>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="storeTokenLocally" id="storeTokenYes" value="yes" defaultChecked />
                        <label className="form-check-label" htmlFor="storeTokenYes">
                            Store token in localStorage
                            </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="storeTokenLocally" id="storeTokenNo" value="no" />
                        <label className="form-check-label" htmlFor="storeTokenNo">
                            <strong>Don't</strong> store token in localStorage
                        </label>
                    </div>
                    <button type="submit" className="mt-2 btn btn-primary" onClick={this.props.onClick}>Submit</button>
                    <br />
                    <OauthSenderDummy />
                    {/*<button type="submit" className="mt-2 btn btn-outline-primary" onClick={(e) => { e.preventDefault(); alert('function coming soon'); }}>Authenticate by OAuth instead</button>*/}

                </div>
            </form>
        </div>
        );
    }
}

export default TokenForm;