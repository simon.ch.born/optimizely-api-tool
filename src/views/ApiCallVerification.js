/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { AddModifiers } from './AddModifiers';
import Axios from 'axios';

export class ApiCallVerification extends Component {
    constructor(props) {
        super(props);

        this.state = {
            requestConfig: {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                },
                method: 'post'
            },
            modifiers: {
            },
            sendApiCallUnlocked: false,
        }

        this.addModifier = this.addModifier.bind(this);
        this.removeModifier = this.removeModifier.bind(this);
        this.assembleOutputData = this.assembleOutputData.bind(this);
        this.sendApiCall = this.sendApiCall.bind(this);

        console.log('on construct');
        console.log(this.props);
    }

    addModifier(formData) {
        console.log('add modifier called');
        let mainObj = this.state.modifiers;
        mainObj[this.props.detailMode] = mainObj[this.props.detailMode] || [];
        mainObj[this.props.detailMode].push({
            'property': formData.selectProperty,
            'find': formData.inputRxMatch,
            'replace': formData.inputRxReplace
        });
        this.setState({ modifiers: mainObj });
    }

    removeModifier(evt) {
        console.log('remove modifier called');
        const idx = evt.target.getAttribute('data-entry-id');
        let mainObj = this.state.modifiers;
        let modifierObj = mainObj[this.props.detailMode];
        modifierObj.splice(idx, 1);
        mainObj[this.props.detailMode] = modifierObj;
        this.setState({ modifiers: mainObj });
    }

    assembleOutputData() {
        let resultArray = [];
        var relevantModifiers = this.state.modifiers[this.props.detailMode];
        if (!relevantModifiers || !relevantModifiers.length) {
            resultArray = this.props.sendArray;
        } else {
            this.props.sendArray.forEach((el, arrIdx) => {
                Object.entries(el).forEach((prop, index) => {
                    relevantModifiers.forEach((mod, modIdx) => {
                        // checking for existent resultArray will allow multiple modifiers on one property
                        if (!resultArray[arrIdx]) {
                            resultArray[arrIdx] = {};
                        }
                        if (mod.property.indexOf(".") === -1) { // if first level property
                            let oldValue = resultArray[arrIdx][prop[0]] || this.props.sendArray[arrIdx][prop[0]];
                            if (mod.property === prop[0]) { // if modifier aims at this property
                                if (oldValue === true || oldValue === false) { // any mod on booleans will toggle true/false
                                    resultArray[arrIdx][prop[0]] = !oldValue;
                                } else {
                                    const rx = new RegExp(mod.find, "g");
                                    resultArray[arrIdx][prop[0]] = oldValue.replace(rx, mod.replace);
                                }
                            } else { // if no modifier applies simply keep value
                                resultArray[arrIdx][prop[0]] = oldValue;
                            }
                        } else { // if mod.property does contain a "." (second level object)
                            let splitModProperty = mod.property.split(".");
                            let propertyToLookForInOrigObj = splitModProperty[0];
                            let propertyToModify = splitModProperty[1];
                            let oldObj = resultArray[arrIdx][prop[0]] || this.props.sendArray[arrIdx][prop[0]];
                            if (propertyToLookForInOrigObj === prop[0]) {
                                let returnObj = {};
                                for (var oldProp in oldObj) {
                                    console.log(`checking for ${propertyToModify} against ${oldProp}`);
                                    if (propertyToModify === oldProp) {
                                        console.log('found mod on property: ' + oldProp);
                                        const rx = new RegExp(mod.find, "g");
                                        returnObj[oldProp] = oldObj[oldProp].replace(rx, mod.replace);
                                    } else {
                                        returnObj[oldProp] = oldObj[oldProp];
                                    }
                                }
                                resultArray[arrIdx][prop[0]] = returnObj;
                            } else {
                                resultArray[arrIdx][prop[0]] = oldObj;
                            }

                        }
                    })
                })
            });
        }
        return resultArray;
    }

    sendApiCall() {
        if (!this.state.sendApiCallUnlocked) {
            var areYouSure = window.confirm('Are you sure you want to send this call (please double check)?\n\rMode is: ' + this.props.callMode + '.\n\rButton is unlocked now, next click will send API call');
            if (areYouSure) {
                this.setState({ sendApiCallUnlocked: true });
            }
            console.log('this.assembleOutputData()');
            console.log(this.assembleOutputData());
        } else {
            console.log('this.assembleOutputData()');
            console.log(this.assembleOutputData());
            let callConfig = this.state.requestConfig;
            callConfig.data = this.assembleOutputData(); /// TODO - make API calls possible for all selected elements on "update"
            console.log('this.props.detailMode: ' + this.props.detailMode);
            console.log('callConfig.data: ');
            console.log(callConfig.data);
            console.log('this.props.urlAppendProperty');
            console.log(this.props.urlAppendProperty);

            console.log('on send request ++++++');
            console.log(this.state);
            console.log(this.props);


            let callUrl = window.globalConf.getApiUrl(this.props.detailMode);

            console.log('call config: ');
            console.log(callConfig);

            if (this.props.callMode === 'update') {

                this.props.selectedEls.forEach((selectedEl, idx) => {

                    let callConfigOnlyElementToUpdate = JSON.parse(JSON.stringify(callConfig));
                    callConfigOnlyElementToUpdate.data = callConfigOnlyElementToUpdate.data[idx]; // pick only data of element to update from callConfig
                    callConfigOnlyElementToUpdate.method = 'PATCH';

                    console.log('this.props.sourceData');
                    console.log(this.props.sourceData);
                    console.log('selectedEl');
                    console.log(selectedEl);

                    let callUrlWithId = callUrl + '/' + this.props.sourceData[selectedEl][this.props.urlAppendProperty[this.props.detailMode]];
                    console.log('callUrlWithId');
                    console.log(callUrlWithId);

                    Axios.request(callUrlWithId, callConfigOnlyElementToUpdate)
                        .then(response => {
                            console.log(response);
                        })
                        .catch(error => {
                            console.error(error);
                        });
                });
            } else if (this.props.callMode === 'create') {

                this.props.selectedEls.forEach((selectedEl, idx) => {

                    let callConfigOnlyElementToCreate = JSON.parse(JSON.stringify(callConfig));
                    callConfigOnlyElementToCreate.data = callConfigOnlyElementToCreate.data[idx]; // pick only data of element to update from callConfig
                    callConfigOnlyElementToCreate.method = 'POST';

                    console.log('this.props.sourceData');
                    console.log(this.props.sourceData);
                    console.log('selectedEl');
                    console.log(selectedEl);

                    console.log('callUrl');
                    console.log(callUrl);


                    console.log('callConfigOnlyElementToCreate');
                    console.log(callConfigOnlyElementToCreate);

                    Axios.request(callUrl, callConfigOnlyElementToCreate)
                        .then(response => {
                            console.log(response);
                        })
                        .catch(error => {
                            console.error(error);
                        });
                });
            }


        }
    }

    render() {

        const outputData = this.assembleOutputData();
        const sendApiClass = this.state.sendApiCallUnlocked ? 'warning' : 'danger';

        return (<div>
            <div className="col-lg-12">
                <h2 className="text-center">Verifying {window.globalConf.firstUppercase(this.props.detailMode)} call (mode: {this.props.callMode})</h2>
            </div>
            <div className="row mb-3 border-bottom pb-2">
                <div className="d-flex justify-content-between col-lg-12">
                    <button className="btn btn-primary" type="submit" onClick={() => this.props.setModeTo(this.props.detailMode)}>&#8592; Back to {window.globalConf.firstUppercase(this.props.detailMode)}</button>
                    <div className="btn-group">
                        <button className={`btn btn-${sendApiClass}`} type="submit" onClick={this.sendApiCall}>Send API {this.props.callMode} call! (click 2x)</button>
                    </div>
                    <button className="btn btn-primary invisible" type="submit"></button>
                </div>
            </div>
            <div>
                <div className="row">
                    <div className="col-lg-6 border-right">
                        <h3>{this.props.callMode}-ing in {this.props.targetProject.name}</h3>
                        <pre><code>{JSON.stringify(outputData, undefined, 4)}</code></pre>
                    </div>
                    <div className="col-lg-6">
                        <AddModifiers
                            sendArray={outputData}
                            addModifier={this.addModifier}
                            removeModifier={this.removeModifier}
                            modifiers={this.state.modifiers}
                            detailMode={this.props.detailMode} />
                    </div>
                </div>
            </div>
        </div>);
    }
}

export default ApiCallVerification;