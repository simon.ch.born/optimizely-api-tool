import React, { Component } from 'react';
import Axios from 'axios';
import Fs from 'fs';

export class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      filename: 'output.json',
      requestConfig: {
        headers: { 'Authorization': `Bearer ${this.props.token}` },
      },
      projectToShow: 0
    }

    this.getData = this.getData.bind(this);
    this.writeToFile = this.writeToFile.bind(this);
    this.nextProj = this.nextProj.bind(this);
    this.prevProj = this.prevProj.bind(this);
    this.showPages = this.showPages.bind(this);
  }

  componentWillMount() {
    this.getData();
  }

  writeToFile(data) {
    Fs.writeFile(this.state.filename, JSON.stringify(data), function (err) {
      if (err) {
        return console.log(err);
      }

      console.log("The file was saved!");
    });
  }

  getData() {
    if (window.globalConf.limitRequests.projects && localStorage.getItem('tmpStoreProjects')) {
      console.log("using local copy of projects");
      console.log(JSON.parse(localStorage.getItem('tmpStoreProjects')));
      this.setState({ data: JSON.parse(localStorage.getItem('tmpStoreProjects')) });
      return;
    } else {
      console.log("requesting remote projects");
      console.log(this.state.requestConfig);
      Axios.get(window.globalConf.getApiUrl('projects', { page: 1, per_page: 100 }), this.state.requestConfig)
        .then(response => {
          console.log(response.data);
          localStorage.setItem('tmpStoreProjects', JSON.stringify(response.data));
          this.setState({ data: response.data });
        })
        .catch(error => {
          console.log(error);
        });
    }
  }

  prevProj() {
    const newProj = this.state.projectToShow === 0 ? this.state.projectToShow : (this.state.projectToShow - 1);
    this.setState({ projectToShow: newProj });
  }

  nextProj() {
    const newProj = this.state.projectToShow < (this.state.data.length - 1) ? (this.state.projectToShow + 1) : (this.state.data.length - 1);
    this.setState({ projectToShow: newProj });
  }

  showPages() {
    const pagesIdToShow = this.state.data[this.state.projectToShow].id;
    console.log(`project to show pages of: ${pagesIdToShow}`);
    this.props.passProject(pagesIdToShow);
  }

  getPagination() {
    const lastShown = this.state.projectToShow === (this.state.data.length - 1);
    const firstShown = this.state.projectToShow === 0;

    return (
      <div className="row mb-3">
        <div className="d-flex justify-content-between col-lg-12">
          <button disabled={firstShown} className="btn btn-primary" type="submit" onClick={this.prevProj}>&#8592; Previous project</button>
          <div className="btn-group">
            <button className="btn btn-primary" onClick={() => { this.props.selectAsSource(this.state.projectToShow, this.state.data) }}>Select as Source Project</button>
            <button className="btn btn-primary" onClick={() => { this.props.selectAsTarget(this.state.projectToShow, this.state.data) }}>Select as Target Project</button>
          </div>
          <div className="btn-group">
            <div className="dropdown mt-1 ml-1">
              <button className="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled={!this.props.anyProjectSelected}>
                Show ...
              </button>
              <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#" onClick={() => { this.props.showProjectDetails('attributes') }}>Attributes</a>
                <a className="dropdown-item" href="#" onClick={() => { this.props.showProjectDetails('audiences') }}>Audiences</a>
                <a className="dropdown-item" href="#" onClick={() => { this.props.showProjectDetails('events') }}>Events</a>
                <a className="dropdown-item" href="#" onClick={() => { this.props.showProjectDetails('pages') }}>Pages</a>

              </div>
            </div>
          </div>
          <button disabled={lastShown} className="btn btn-primary" type="submit" onClick={this.nextProj}>Next project &#8594;</button>
        </div>
      </div>
    );
  }

  render() {

    const dataAvailable = this.state.data.length !== 0;
    const content = !dataAvailable ? '...Loading' : JSON.stringify(this.state.data[this.state.projectToShow], undefined, 4)/*.replace(/\:\s\"/g,': <input type="text" value="').replace(/\",/g,'" />')*/;
    const pagination = dataAvailable ? this.getPagination() : null;
    const projectName = dataAvailable ? this.state.data[this.state.projectToShow].name : '';

    return (
      <div>
        <h2 className="text-center mb-2 border-bottom">Projects</h2>
        {pagination}
        <div className="row">
          <div className="col-lg-12">
            <h2>{projectName}</h2>
            <pre className="text-left"><code>{content}</code></pre>
          </div>
        </div>
      </div>
    );
  }
}

export default Projects;