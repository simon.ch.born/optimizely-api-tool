/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { SelectableList } from './SelectableList';

export class InfoAndSelection extends Component {
    constructor(props) {
        super(props);
        console.log(this.props);
        console.log(this.props);

        this.state = {
            showOnlyOneProject: !this.props.targetData || (this.props.targetProject.id === this.props.sourceProject.id)

        }

        console.log(this.state);

        this.getSecondColumn = this.getSecondColumn.bind(this);
    }

    getSecondColumn() {
        return this.state.showOnlyOneProject ? '' : (
            <div className="col-lg-6">
                <h3>{this.props.targetProject.name}</h3>
                <SelectableList
                    data={this.props.targetData}
                    mode={this.props.detailMode}
                    selectionAllowed={false} />
            </div>
        );
    }

    render() {
        let firstColWidth = !this.state.showOnlyOneProject ? 'col-lg-6' : 'col-lg-12';
        return (
            <div>
                <div className="col-lg-12">
                    <h2 className="text-center">{window.globalConf.firstUppercase(this.props.detailMode)}</h2>
                </div>
                <div className="row mb-3 border-bottom pb-2">
                    <div className="d-flex justify-content-between col-lg-12">
                        <button className="btn btn-primary" type="submit" onClick={() => this.props.backFunc('Projects')}>&#8592; Back to Projects</button>
                        <div className="btn-group">
                            <button disabled={!this.props.selectedEls.length || !this.props.targetProject.id} className="btn btn-primary" type="submit" onClick={this.props.create}>Configure "create" call to "{this.props.targetProject.name}"</button>
                            <button disabled={/*(this.props.sourceProject.id !== this.props.targetProject.id) ||*/ (this.props.selectedEls.length === 0)} className="btn btn-primary" type="submit" onClick={this.props.update}>Configure "update" call to "{this.props.sourceProject.name}"</button>
                        </div>
                        <div className="btn-group">
                            <div className="dropdown mr-1">
                                <button className="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Show ...
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a className="dropdown-item" href="#" onClick={() => { this.props.setModeTo('attributes') }}>Attributes</a>
                                    <a className="dropdown-item" href="#" onClick={() => { this.props.setModeTo('audiences') }}>Audiences</a>
                                    <a className="dropdown-item" href="#" onClick={() => { this.props.setModeTo('events') }}>Events</a>
                                    <a className="dropdown-item" href="#" onClick={() => { this.props.setModeTo('pages') }}>Pages</a>
                                </div>
                            </div>
                        </div>
                        <button className="btn btn-primary invisible" type="submit"></button>
                    </div>
                </div>
                <div>
                    <div className="row">
                        <div className={firstColWidth}>
                            <h3>{this.props.sourceProject.name}</h3>
                            <SelectableList
                                data={this.props.sourceData}
                                mode={this.props.detailMode}
                                selectionAllowed={true}
                                selectAll={this.props.selectAll}
                                unselectAll={this.props.unselectAll}
                                toggleSelection={this.props.toggleSelection}
                                selectedEls={this.props.selectedEls} />
                        </div>
                        {this.getSecondColumn()}
                    </div>
                </div>
            </div>
        );
    }
}

export default InfoAndSelection;