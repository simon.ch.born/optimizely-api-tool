import React, { Component } from 'react';

export class Settings extends Component {
    constructor(props) {
        super(props);

        this.getBetterActiveClass = this.getBetterActiveClass.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    getBetterActiveClass(active) {
    }

    handleSubmit() {
        this.props.switchModeTo('TokenForm');
    }

    render() {
        return (<div className="row">
            <div className="col-lg-12">
                <form onSubmit={this.handleSubmit} >
                    <div className="btn-group btn-group-toggle mr-2" data-toggle="buttons">
                        <label className={`btn btn-${this.props.includeArchived ? 'success active' : 'secondary'}`} onClick={this.props.toggleIncArchived}>
                            <input type="radio" name="options" id="option1" autoComplete="off" defaultChecked={this.props.includeArchived} />Yes</label>
                        <label className={`btn btn-${!this.props.includeArchived ? 'success active' : 'secondary'}`} onClick={this.props.toggleIncArchived}>
                            <input type="radio" name="options" id="option2" autoComplete="off" defaultChecked={!this.props.includeArchived} />No</label>
                    </div>
                    <span className="pull-left">Show archived data and include in copying</span>
                    <div className="row">
                        <div className="col-lg-12">
                            <button className="btn btn-primary mt-5" type="submit">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        );
    }
}

export default Settings;