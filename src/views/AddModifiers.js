import React, { Component } from 'react';

export class AddModifiers extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputRxMatch: '',
            inputRxReplace: '',
            selectProperty: Object.entries(this.props.sendArray[0])[0][0],
        }

        this.getSelectOptions = this.getSelectOptions.bind(this);
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getAppliedMods = this.getAppliedMods.bind(this);
    }

    getSelectOptions() {
        return Object.entries(this.props.sendArray[0]).map(function (el, index) {
            if (el[0] === 'project_id') {
                return void(0);
            }
            if (typeof el[1] !== 'object') {
                return (<option key={index}>{el[0]}</option>);
            } else {
                let returnArr = [];
                for (var prop in el[1]) {
                    returnArr.push(<option key={`${index}_${prop}`}>{el[0]}.{prop}</option>);
                }
                return returnArr;
            }
        });
        //return false;
    }

    handleFieldChange(evt) {
        this.setState({ [evt.target.id]: evt.target.value });
    }

    handleSubmit(evt) {
        evt.preventDefault();
        // somehow had problems passing this directly... (?)
        this.props.addModifier(this.state);
        this.setState({
            inputRxMatch: '',
            inputRxReplace: '',
            selectProperty: Object.entries(this.props.sendArray[0])[0][0],
        });
    }

    getAppliedMods() {
        if (!this.props.modifiers.hasOwnProperty(this.props.detailMode) || !this.props.modifiers[this.props.detailMode].length) { return '' }
        else {
            let modsArray = [];
            for (var i = 0; i < this.props.modifiers[this.props.detailMode].length; i++) {
                const el = this.props.modifiers[this.props.detailMode][i];
                modsArray.push(<div key={i} className="row border-bottom pb-3 mb-3"><div className="col-lg-2"><button className="btn btn-danger" data-entry-id={i} onClick={this.props.removeModifier}>Remove</button></div><div className="col-lg-10"><pre>{el.property}: "{el.find}" -> "{el.replace}"</pre></div></div>)
            }
            return (<div><h3>Applied Modifiers</h3>{modsArray}</div>);
        }
    }

    render() {

        const appliedMods = this.getAppliedMods();

        return (<div>
            {appliedMods}
            <h3>Add Modifier</h3>
            <form onSubmit={this.handleSubmit}>
                <div className="row">
                    <div className="col-lg-4">
                        <div className="form-group">
                            <label htmlFor="selectProperty">Select Property</label>
                            <select className="form-control" value={this.state.selectProperty} id="selectProperty" onChange={e => this.handleFieldChange(e)}>
                                {this.getSelectOptions()}
                            </select>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="form-group">
                            <label htmlFor="inputRxMatch">RegEx matching</label>
                            <input type="text" onChange={e => this.handleFieldChange(e)} value={this.state.inputRxMatch} className="form-control" id="inputRxMatch" aria-describedby="regexMatchHelp" placeholder="Enter a regex for this field" />
                            <small id="regexMatchHelp" className="form-text text-muted">For example .* or ^[0-9]+.*$ - match will be global</small>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="form-group">
                            <label htmlFor="inputRxReplace">RegEx replacer</label>
                            <input type="text" onChange={e => this.handleFieldChange(e)} value={this.state.inputRxReplace} className="form-control" id="inputRxReplace" aria-describedby="inputRxHelp" placeholder="Enter a replace value" />
                            <small id="inputRxHelp" className="form-text text-muted">Replace value - use $1,$2... for group reference; $& for entire string</small>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <input className="btn btn-primary" type="submit" disabled={!this.state.inputRxMatch.length} value="Add modifier" />
                    </div>
                </div>
            </form>

        </div>);
    }
}

export default AddModifiers;