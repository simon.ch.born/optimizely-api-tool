import React, { Component } from 'react';
import Axios from 'axios';

export class Attributes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            requestConfig: {
                headers: { 'Authorization': `Bearer ${this.props.token}` }
            },
        }
    }

    componentWillMount() {
        this.getData();
    }

    getData() {
        if (window.globalConf.dontRequestAttributes && localStorage.getItem('tmpStoreAttributes')) {
            this.setState({ data: JSON.parse(localStorage.getItem('tmpStoreAttributes')) });
            return;
        } else {
            Axios.get(window.globalConf.optUrls.attributes + '?project_id=' + this.props.projectId, this.state.requestConfig)
                .then(response => {
                    localStorage.setItem('tmpStoreAttributes', JSON.stringify(response.data));
                    this.setState({ data: response.data });
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }

    render() {

        const dataAvailable = this.state.data.length !== 0;
        const content = !dataAvailable ? '...Loading' : JSON.stringify(this.state.data, undefined, 4);

        return (
            <div>
                <div className="col-lg-12">
                    <h2 className="text-center mt-5">Attributes for Project {this.props.projectId}</h2>
                </div>
                <div className="row mb-3">
                    <div className="d-flex justify-content-between col-lg-12">
                        <button className="btn btn-primary" type="submit" onClick={this.props.backFunc}>&#8592; Back to Projects</button>
                        <div>
                            <button className="btn btn-primary mr-1" onClick={(e) => this.props.switchMode('Pages')}>Show pages</button>
                            <button className="btn btn-primary" onClick={(e) => this.props.switchMode('Audiences')}>Show audiences</button>
                        </div>
                        <button className="btn btn-primary" type="submit"></button>
                    </div>
                </div>
                <div>
                    <div className="row">
                        <div className="col-lg-12">
                            <pre className="text-left"><code>{content}</code></pre>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Attributes;