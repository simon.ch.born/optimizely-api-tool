function handleFeedback(){
    document.body.classList.add('hideFeedbackArea');
    document.getElementById('feedbackOpener').textContent = "THANKS !";
    window.setTimeout(function(){
        document.getElementById('feedbackOpener').textContent = "FEEDBACK";

    },4000);
}

var feedbackEl = document.createElement('div');
feedbackEl.id = 'feedbackArea';
var innerHtml = '<div style="inline-block; text-align: right;"><span onClick="(function(){document.body.classList.add(\'hideFeedbackArea\')})()" style="cursor:pointer; display: inline-block; margin-bottom: 10px">CLOSE X</span></div><textarea style="height: 75%; width: 90%" id="feedbackContent"></textarea><br /><button style="color: #fff; background-color: #007bff;" onClick="handleFeedback()" class="feedbackButton">Send Feedback</button>';
feedbackEl.innerHTML = innerHtml;
document.body.appendChild(feedbackEl);

var feedbackOpener = document.createElement('span');
feedbackOpener.id = 'feedbackOpener';
feedbackOpener.setAttribute('style', 'position: absolute; bottom: 1%; right: 0%; padding: 10px; font-size:22px; font-weight: bold; writing-mode: vertical-rl; text-orientation: mixed; border: 1px solid darkgrey; background: #eee; cursor:pointer;');
feedbackOpener.textContent = 'FEEDBACK';
feedbackOpener.onclick = function(){document.body.classList.remove('hideFeedbackArea');document.body.classList.add('showFeedbackArea')};
document.body.appendChild(feedbackOpener);